FROM alpine:edge

ENV NAME=succulent VERSION=0 ARCH=x86_64

WORKDIR /succulent-app

ENV PACKAGES="\
    py3-succulent \
    py3-gunicorn \
"

RUN apk update \
    && apk upgrade && apk add --no-cache \
    python3 \
    python3-dev \
    $PACKAGES \
    && rm -rf /var/cache/apk/*

CMD ["sh"]
